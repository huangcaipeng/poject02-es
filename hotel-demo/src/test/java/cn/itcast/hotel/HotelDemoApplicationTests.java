package cn.itcast.hotel;

import cn.itcast.hotel.constants.HotelConstants;
import cn.itcast.hotel.pojo.Hotel;
import cn.itcast.hotel.pojo.HotelDoc;
import cn.itcast.hotel.service.IHotelService;

import com.alibaba.fastjson.JSON;
import org.apache.http.HttpHost;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.get.GetRequest;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.client.indices.CreateIndexRequest;
import org.elasticsearch.client.indices.GetIndexRequest;
import org.elasticsearch.common.xcontent.XContentType;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;
import java.util.List;

@SpringBootTest
class HotelDemoApplicationTests {
    @Autowired
    private IHotelService hotelService;

    @Autowired
    private RestHighLevelClient client;




    @Test
    void contextLoads() {
        System.out.println(client);
    }
    @Test
    void createHotelIndex() throws IOException {
//        1.创建Request对象
        CreateIndexRequest request = new CreateIndexRequest("hotel");


//        2. 准备请求的参数：DSL语句
        request.source(HotelConstants.MAPPING_TEMPLATE, XContentType.JSON);
//        3.发送情求
        client.indices().create(request, RequestOptions.DEFAULT);
    }
    @Test
    void testExistsHotelIndex() throws IOException {
        // 1.创建Request对象
        GetIndexRequest request = new GetIndexRequest("hotel");
        // 2.发送请求
        boolean exists = client.indices().exists(request, RequestOptions.DEFAULT);
        // 3.输出
        System.err.println(exists ? "索引库已经存在！" : "索引库不存在！");
    }
    @BeforeEach
    void setUp(){
        this.client=new RestHighLevelClient(RestClient.builder(HttpHost.create("http://192.168.150.101:9200")));
    }
    @AfterEach
    void tearDown() throws IOException {
        this.client.close();
    }
    @Test
    void testAddDocument() throws IOException {
        //1.根据id查询酒店数据
        Hotel hotel = hotelService.getById(61083L);
//        2.转成文档类型
        HotelDoc hotelDoc = new HotelDoc(hotel);
//        3.将HotelDoc转换成json
        String json = JSON.toJSONString(hotelDoc);
//        4.准备Request对象
        IndexRequest request = new IndexRequest("hotel").id(hotel.getId().toString());
//        5.准备JSON文档
        request.source(json,XContentType.JSON);
//        6.发送请求
        client.index(request,RequestOptions.DEFAULT);
    }
    @Test
    void testGetDocumentById() throws IOException {
//        1.准备request
        GetRequest request = new GetRequest("hotel","61083");
//        2.发送请求,得到响应
        GetResponse response = client.get(request,RequestOptions.DEFAULT);
//        3.解析响应结果
        String json = response.getSourceAsString();
        HotelDoc hotelDoc=JSON.parseObject(json,HotelDoc.class);
        System.out.println(hotelDoc);
    }
    @Test
    void testBulkRequest() throws IOException {
        //批量查询酒店数据
        List<Hotel> hotels = hotelService.list();
        //1.创建Request
        BulkRequest request = new BulkRequest();
        //2. 准备参数，添加多个新增的Request
        for (Hotel hotel : hotels) {
            //2.1转换成文档类型HoteLDoc
            HotelDoc hotelDoc = new HotelDoc(hotel);
            //2.2创建新增文档的Request对象
            request.add(new IndexRequest("hotel")
            .id(hotelDoc.getId().toString())
            .source(JSON.toJSONString(hotelDoc),XContentType.JSON));

        }
        //3.发送请求
        client.bulk(request,RequestOptions.DEFAULT);
    }





}
